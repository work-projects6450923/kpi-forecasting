## Key Performance Indicator Forecasting

## Model Details

Owning Team: Connect Data Science

Point of Contact: Meghnath Reddy Challa

Version

## Description

Forecast daily level Key Performance Metrics (Quotes, Responses, Sales, Renewal WrittenPremium, NewBusiness WrittenPremium) for different sources of Business (Costco, Advisor & Others) at different Line of Business (Home, Auto & Umbrella) and different Channel conclusions (Call Center, Internet).

## AI Canvas

![AI Canvas.png](./AI Canvas.png)

## Success Metrics

Mean Absolute Percentange Error is considered as measure of success

## Data Collection

The model is trained on historical data ranging from January 1st,2019 to December 31st,2021. 
SQL Script used to extract data from Prem can be found in this repo [GitLab](https://gitlab.com/work-projects6450923/kpi-forecasting/-/tree/main/SQL%20Scripts)

Date - 'mm-dd-yyyy'

SourceofBusiness - Advisor, Costco, Other&Non-Client

LineofBusiness - Auto, Home

IntakeChannelConclusion

> Call Center - Call Center

> Internet - Call Center
 
> Internet - Internet

Issuance Type

> New Business

> Renewal

## Independent Variables

Day of the week

Month

is_public_holiday (0 or 1)

Lag variable - has last 30days target variable values

## Target Variables

Quotes Count

Response Count

Sales Count

New Business Written Premium ($)

Renewal Written Premium ($)


## Assumptions

> Source of Business other than Costco and Advisor are treated under the category of “Others”.

> Different Channel Conclusions have different Operational behavior (working hours), making sure they are tackled the same in test and train data.

> For Umbrella, only monthly level data is available for Quotes and Responses.

## Modeling Plan

For Home & Auto - Forecast Quotes, Responses, and Sales

For Umbrella - Forecast Sales for different Source of Business

For Written Premium - Forecast for different Source of Business at a daily level

## Custom functions

split_data() : Last 3 months of data (2022 - October, November, December) is allocated for test data and the rest for training data.

create_shape() : Reshape input data to be three-dimensional [samples, timesteps, features].

Embedding layer, Numerical layer and LSTM layers are consolidated to form a multiple hidden layers to make the model deeper and accurate.

Metric functions (plot_prediction(), metric_zero(), metric_nonzero()) are used to display the success matrix.

## Deliverables

Annual Forecasting Values

![Model_Forecast.png](./Model_Forecast.png)

KPI Dashboard

![KPI Forecast Dashboard.png](./KPI Forecast Dashboard.png)