# KPI Time Series Forecasting

This project focuses on building a forecasting model to predict key metrics, such as Responses, Quotes, Sales, and Premium Amount, for CONNECT book of business. The Market Management team provides a monthly level forecast for these metrics, but there is currently no daily level forecasting model.

# Problem Statement

CONNECT has different sources of business and channels, including Call Center and Internet, for Auto and Home Policies. It is important to have different models for each of these scenarios because each category has its own seasonality and trends. 

For Example: Call Center services are closed on weekends which impacts the Sales volume whereas we see a high volume for Internet during the weekends. Similarly Written Premium collected is high on the 1st and 15th of every month since most of the customers pay their premium during these days. So, features such as "is_weekend", "is_month_start", "is_mid_month", and "is_public_holiday" are included in the model to learn these patterns.

However, there are some issues with historical peak day trends that might not apply to the forecast year. For example, a "Marketing Campaign" may have run on January 15th, 2021, causing a spike in sales, but we do not expect to see the same number for the forecast year. Therefore, an additional feature called "campaign_indicator" is included, and the corresponding KPI value is set to "0". This way, the model eliminates bias for the campaign dates while making future projections.

# Models Used
Used three different models:

XGBoost Regressor
Facebook's Prophet
LSTM Model
The LSTM Model performed better with the least MAPE value.

# Dashboard

![KPI Forecast Dashboard.png](./KPI%20Forecast%20Dashboard.png)