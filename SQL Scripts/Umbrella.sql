with table1 as 
(
SELECT
[DayDate]
, sob.[SourceofBusinessPrimaryDesc] 
, [LineofBusinessDesc]
---Considering "Call Center-Internet" as "Internet-Internet"
,CASE CONCAT(pic.[Channel], '-', pic.[ChannelConclusion]) WHEN 'Call Center-Internet' then 'Internet-Call Center'
ELSE CONCAT(pic.[Channel], '-', pic.[ChannelConclusion])  END IntakeChannelCoclusion 
,COALESCE(SUM([NewBusinessIssuedCnt]),0) NewBusinessIssuedCnt

FROM [BusinessWarehouse].[Warehouse].[fProductionPolicyDaily] f

LEFT JOIN [Warehouse].[dSourceofBusiness] sob on f.[SourceofBusinessWID] = sob.[SourceofBusinessWID]
LEFT JOIN [Warehouse].[dDate] d on f.[ActivityDateWID] = d.[DateWID]
LEFT JOIN [Warehouse].[dPolicyCoverage] pc on f.[PolicyCoverageWID] = pc.[PolicyCoverageWID]
LEFT JOIN [Warehouse].[dPolicyIssuanceCharacteristic] pic on f.PolicyIssuanceCharacteristicWID = pic.PolicyIssuanceCharacteristicWID

WHERE d.[TodayOffset] < 1
AND DayDate >= '2018-12-31 00:00:00'
AND DayDate <= '2021-12-31 00:00:00'
AND sob.[SourceofBusinessPrimaryDesc] = 'Costco'
AND  [LineofBusinessDesc] = 'Umbrella' 

GROUP BY
[DayDate]
,pc.[LineofBusinessDesc]	
, sob.[SourceofBusinessPrimaryDesc] 
,CASE CONCAT(pic.[Channel], '-', pic.[ChannelConclusion]) WHEN 'Call Center-Internet' then 'Internet-Call Center'
ELSE CONCAT(pic.[Channel], '-', pic.[ChannelConclusion]) END

)

select  [DayDate],SourceofBusinessPrimaryDesc,[LineofBusinessDesc],IntakeChannelCoclusion
,COALESCE(SUM([NewBusinessIssuedCnt]),0) NewBusinessIssuedCnt

from table1
group by [DayDate],[LineofBusinessDesc],SourceofBusinessPrimaryDesc,IntakeChannelCoclusion
ORDER BY 
[DayDate],
[LineofBusinessDesc] asc,
IntakeChannelCoclusion asc