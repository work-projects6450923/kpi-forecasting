WITH table1 AS (
SELECT
BEGIN_DATE
,CASE 
WHEN AGENCY_ID = 'CO001001001' THEN 'Costco'
WHEN AGENCY_ID = 'AX001001001' OR AGENCY_ID = 'AD001001001' THEN 'Advisor'
ELSE 'Other&Non-Client'
END "Source of Business Primary"
,CASE (POL_TYPE) WHEN 'H' THEN 'Home' WHEN '3' THEN 'Auto' WHEN 'U' THEN 'Umbrella' ELSE 'N/A' END LineofBusinessDesc
,CONCAT(CONCAT((CASE CHANNEL_INITIAL WHEN '1' THEN 'Call Center' ELSE 'Internet' END),'-'),
(CASE CHANNEL_CONCLUSION WHEN '1' THEN 'Call Center' ELSE 'Internet' END)) CHANNEL_CONCLUSION
,SUM(wrtn_prem_amt) wrtn_prem_amt
FROM RPTADM.DAILY_WRTN_PREM
WHERE BEGIN_DATE >= to_date('2019-01-01', 'YYYY-MM-DD')
AND BEGIN_DATE < to_date('2022-01-01', 'YYYY-MM-DD')
GROUP BY
BEGIN_DATE
,POL_TYPE
,AGENCY_ID
,CONCAT(CONCAT((CASE CHANNEL_INITIAL WHEN '1' THEN 'Call Center' ELSE 'Internet' END),'-'),
(CASE CHANNEL_CONCLUSION WHEN '1' THEN 'Call Center' ELSE 'Internet' END))
)

SELECT DISTINCT BEGIN_DATE,"Source of Business Primary",LineofBusinessDesc AS "LineofBusinessDesc",
CASE CHANNEL_CONCLUSION WHEN 'Call Center-Internet' then 'Internet-Call Center' ELSE CHANNEL_CONCLUSION END AS "IntakeChannelCoclusion",
sum(wrtn_prem_amt) OVER (PARTITION BY "Source of Business Primary",BEGIN_DATE,LineofBusinessDesc,CHANNEL_CONCLUSION) AS wrtn_prem_amt
FROM table1